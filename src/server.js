import sirv from 'sirv';
import express from 'express';
import compression from 'compression';
import * as sapper from '@sapper/server';

var Gun = require('gun');
require('gun/axe');

let gun = null;

Gun.on('opt', function(ctx){
	if(ctx.once){ return }
	ctx.on('in', function(msg){
	  var to = this.to;
	  console.log("Handling msg : " + JSON.stringify(msg, null, ' '));
	  // process message.
	  to.next(msg); // pass to next middleware
	});
});
  
const {
	PORT,
	NODE_ENV
} = process.env;
const dev = NODE_ENV === 'development';
var port = 1337;

var app = express()

app.use(compression({
	threshold: 0
}))

app.get("/server_graph", function(req, res){
	var ret = null;
	if(!gun) {
		res.status(200).send([]);
	} else {
		res.status(200).send(Object.keys(gun._.graph));
	}
})

app.use(sirv('static', {
	dev
}))

app.use(sapper.middleware())

var server = app.listen(PORT, function (err) {
	if (err) console.log('error', err);
})

app.use(Gun.serve);

gun = Gun({
	file: 'data',
	web: server
});

