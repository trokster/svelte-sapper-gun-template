function watchNode(soul, parsed){

    if(parsed === undefined) parsed = [];
    
    if(parsed.indexOf(soul) != -1) {
        //console.log("Bailing out of circular ref: " + soul);
        return;
    }
    // log parsed values so we don't go circular
    parsed.push(soul);

    console.log("Received request to watch: " + soul);

    addNode(soul, null, 15);

    gun.get(soul).on(function(node){
        if(!node) {
            console.log("Heard nullification of: " + soul);
            // Delete node and links ?

        }
    });

    gun.get(soul).once(function(node){

        var pub = getPub(soul);
        var nodes = node;
        Object.keys(node).forEach(function(key){
            if(key == "_") return;
            var node = nodes[key];
            console.log("Checking if node: [" + Gun.node.is(node) + "] :: " + key + " ::" + JSON.stringify(node, null, ' '));
            if(Gun.node.is(node)) {
                // Create node
                addNode(soul+"::prop::"+key, key, 10);
                // add link
                addLink(soul, soul+"::prop::"+key);

                // link and watch it but from another stack
                // ( to avoid too deep recursing )
                setTimeout(function(){
                    //console.log("Run timed out request: " + Gun.node.soul(node) + " :: " + JSON.stringify(parsed));
                    watchNode(Gun.node.soul(node), parsed);
                }, 0);
                addLink(soul+"::prop::"+key, Gun.node.soul(node));
            } else if(node["#"]) {
                if(nodeAlreadyWatched(node["#"])) {
                    if(nodeAlreadyWatched(soul+"::prop::"+key)) {
                        var tmp = nodeAlreadyWatched(soul+"::prop::"+key);
                        tmp.collapsed = false;
                        addLink(soul+"::prop::"+key, node["#"]);
                    } else {
                        addNode(soul+"::prop::"+key, key, 10, false);
                        addLink(soul, soul+"::prop::"+key);
                        addLink(soul+"::prop::"+key, node["#"]);
                    }
                } else {
                    addNode(soul+"::prop::"+key, key, 10, true, node["#"]);
                    addLink(soul, soul+"::prop::"+key);
                }
            } else {
                // Create node
                addNode(soul+"::prop::"+key, key, 10);
                // It's a leaf ( add prop value )
                addNode(soul+"::prop::"+key+"::leaf", node, 10);
                addLink(soul+"::prop::"+key, soul+"::prop::"+key+"::leaf");
                                    // add link
                addLink(soul, soul+"::prop::"+key);

            }
        })

    });

}



function addNode(id, value, radius, collapsed, nodename) {
    // Do not add if it exists but update value if necessary
    var node = null;

    if(!collapsed) collapsed = false;
    if(!nodename) nodename = null;

    if(node = nodes.filter(function(item){return item.id == id}).pop()) {
        if(value !== undefined && value !== null) {
            node.value = value;
        }
        return;
    }
    node = {id:id, radius: radius, collapsed: collapsed, nodename: nodename, value:value};
    if(value !== undefined && value !== null) {
        node.value = value;
    }
    console.log("Creating node: " + id);
    nodes.push(node);
    restartSimulation();
}
